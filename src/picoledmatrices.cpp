#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"

#include "libraries/pico_graphics/pico_graphics.hpp"
#include "galactic_unicorn.hpp"
#include "okcolor.hpp"

using namespace pimoroni;

#define PLM_H 11
#define PLM_L 53

PicoGraphics_PenRGB888 graphics(PLM_H, PLM_L, nullptr);
GalacticUnicorn galactic_unicorn;

float lifetime[PLM_H][PLM_L];
float age[PLM_H][PLM_L];

uint8_t r_color[PLM_H][PLM_L];
uint8_t g_color[PLM_H][PLM_L];
uint8_t b_color[PLM_H][PLM_L];

uint8_t color_from_buttons()
{
  uint8_t val = 0;
  val = val | (((uint8_t)galactic_unicorn.is_pressed(galactic_unicorn.SWITCH_A)) << 0);
  val = val | (((uint8_t)galactic_unicorn.is_pressed(galactic_unicorn.SWITCH_B)) << 1);
  val = val | (((uint8_t)galactic_unicorn.is_pressed(galactic_unicorn.SWITCH_C)) << 2);
  val = val | (((uint8_t)galactic_unicorn.is_pressed(galactic_unicorn.SWITCH_D)) << 3);
  return val;
}

const uint8_t color_list[16*3]={
  230,  150,  0,    // default
  255,  0,    0, 
  0,    255,  0,
  0,    0,    255,
  255,  255,  0,
  0,    255,  255,
  255,  0,    255,
  0,    250,  160,
  160,  0,    250,
  160,  250,  0,
  0,    160,  250,
  250,  250,  250,
  128,  128,  128,
  64,   128,  57,
  200,  34,   57,
  0,    0,    0
};

void set_color_from_buttons(uint8_t x, uint8_t y)
{
  uint8_t val = 0;
  val = color_from_buttons();

  if (val > 15)
  {
    val = 16;
  }

  if (val == 15)
  {
    r_color[x][y] = rand();
    g_color[x][y] = rand();
    b_color[x][y] = rand();
  }
  else
  {
    
    r_color[x][y] = color_list[3*(val)+0];
    g_color[x][y] = color_list[3*(val)+1];
    b_color[x][y] = color_list[3*(val)+2];
  }
}

int main()
{

  stdio_init_all();

  // initialize random age and lifetime for each pixel
  for (int y = 0; y < PLM_L; y++)
  {
    for (int x = 0; x < PLM_H; x++)
    {
      lifetime[x][y] = 1.0f + ((rand() % 10) / 100.0f);
      age[x][y] = ((rand() % 100) / 100.0f) * lifetime[x][y];
      r_color[x][y] = 230;
      g_color[x][y] = 150;
      b_color[x][y] = 0;
    }
  }

  // init the unicorn. set up pins, pio
  galactic_unicorn.init();
  galactic_unicorn.set_brightness(.5);
  while (true)
  {
    if (galactic_unicorn.is_pressed(galactic_unicorn.SWITCH_BRIGHTNESS_UP))
    {
      galactic_unicorn.adjust_brightness(+0.01);
    }
    if (galactic_unicorn.is_pressed(galactic_unicorn.SWITCH_BRIGHTNESS_DOWN))
    {
      galactic_unicorn.adjust_brightness(-0.01);
    }

    graphics.set_pen(0, 0, 0);
    graphics.clear();

    for (int y = 0; y < PLM_L; y++)
    {
      for (int x = 0; x < PLM_H; x++)
      {
        if (age[x][y] < lifetime[x][y] * 0.3f)
        {
          graphics.set_pen(r_color[x][y], g_color[x][y], b_color[x][y]);
          //graphics.set_pen(230, 150, 0);
          graphics.pixel(Point(x, y));
        }
        else if (age[x][y] < lifetime[x][y] * 0.5f)
        {
          float decay = (lifetime[x][y] * 0.5f - age[x][y]) * 5.0f;
          graphics.set_pen(decay * r_color[x][y], decay * g_color[x][y], decay * b_color[x][y]);
          //graphics.set_pen(decay * 230, decay * 150, 0);
          graphics.pixel(Point(x, y));
        }

        if (age[x][y] >= lifetime[x][y])
        {
          age[x][y] = 0.0f;
          lifetime[x][y] = 1.0f + ((rand() % 10) / 100.0f);
          set_color_from_buttons(x,y);
        }

        age[x][y] += 0.01f;
      }
    }

    //push to display
    galactic_unicorn.update(&graphics);

    sleep_ms(10);
  }

  return 0;
}
