cmake_minimum_required(VERSION 3.12)

# Pull in PICO SDK (must be before project)
include(pico_sdk_import.cmake)

project(picoledmatrices C CXX ASM)
include(pimoroni_pico_import.cmake)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

# Initialize the SDK
pico_sdk_init()

add_executable(
  ${PROJECT_NAME}
  src/picoledmatrices.cpp
)

# include(${PIMORONI_PICO_PATH}/common)
# include(${PIMORONI_PICO_PATH}/libraries)
# include(${PIMORONI_PICO_PATH}/drivers)


include(${PIMORONI_PICO_PATH}/common/pimoroni_i2c.cmake)
include(${PIMORONI_PICO_PATH}/common/pimoroni_bus.cmake)
include(${PIMORONI_PICO_PATH}/libraries/pico_graphics/pico_graphics.cmake)
include(${PIMORONI_PICO_PATH}/libraries/galactic_unicorn/galactic_unicorn.cmake)

# Pull in pico libraries that we need
target_link_libraries(
  ${PROJECT_NAME}
  pico_stdlib
  hardware_pio
  hardware_adc
  hardware_dma
  pico_graphics
  galactic_unicorn)

  pico_enable_stdio_usb(${PROJECT_NAME} 1)

# create map/bin/hex file etc.
pico_add_extra_outputs(${PROJECT_NAME})
